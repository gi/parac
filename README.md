# ParaC

220 genomes from the _Salmonella enterica_ Para C Lineage.

Published in / cite:

- Zhemin Zhou, Nabil-Fareed Alikhan, Martin J. Sergeant, Nina Luhmann, Cátia Vaz, Alexandre P. Francisco, João André Carriço and Mark Achtman. "GrapeTree: visualization of core genomic relationships among 100,000 bacterial pathogens" Genome Res. 2018. 28: 1395-1404.

Figure 5 of the above publication shows phylogenies based on 220 assemblies. The assmeblies have been published on EnteroBase (http://enterobase.warwick.ac.uk/).

Not all of these assemblies are available on EnteroBase anymore and bulk download on command line is currently not possible.

Here, we provide a snapshot of the published data from 02/2019 for reasons of reproducibility of experiments that have been performed on that data meanwhile, e.g. in

- Tizian Schulz, Roland Wittler, Sven Rahmann, Faraz Hach, Jens Stoye. "Detecting High Scoring Local Alignments in Pangenome Graphs" Bioinformatics. (2021)
- Roland Wittler "Alignment- and reference-free phylogenomics with colored de Bruijn graphs" Algorithms for Molecular Biology. 2020. 15: 4.

For completeness, we also provide the phylogenetic tree shown in Figure 5A of the above manuscript, originally provided here: http://bit.ly/2vuFIIb. The file list `ms_list.txt` can e.g. be used as input for [SANS](https://gitlab.ub.uni-bielefeld.de/gi/sans) after decompressing the fasta files.

This provision of the data has been permitted by the EnteroBase development team. We thank the team for their valuable help.

When using this data, cite the original publication:

- Zhemin Zhou, Nabil-Fareed Alikhan, Martin J. Sergeant, Nina Luhmann, Cátia Vaz, Alexandre P. Francisco, João André Carriço and Mark Achtman. "GrapeTree: visualization of core genomic relationships among 100,000 bacterial pathogens" Genome Res. 2018. 28: 1395-1404.
